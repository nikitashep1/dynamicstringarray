public class Application {
	public static void main(String[] args) {
		DynamicStringArray arr = new DynamicStringArray();
		
		System.out.println("Pop Method");
		System.out.println("Removes and returns the last element of the array");
		System.out.println(arr);
		String popped = arr.pop();
		System.out.println(arr);
		System.out.println("Popped element: " + popped);
		
		arr.reset();
		
		System.out.println("\nPush Method");
		System.out.println("Adds an element to the end of the array and return the new array length. REQUIRES PARAMETER");
		System.out.println(arr);
		String pushed = "eleven";
		int newArrLength = arr.push(pushed);
		System.out.println(arr);
		System.out.println("Pushed element: " + pushed);
		System.out.println("Returns " + newArrLength);
		
		arr.reset();
		
		System.out.println("\nShift Method");
		System.out.println("Removes and returns the first element of the array");
		System.out.println(arr);
		String shifted = arr.shift();
		System.out.println(arr);
		System.out.println("Shifted element: " + shifted);
		
		arr.reset();
		
		System.out.println("\nUnshift Method");
		System.out.println("Adds an element to the beginning of the array and returns the new length of the array. REQUIRES PARAMETER");
		System.out.println(arr);
		String unshifted = "zero";
		int newArrLength2 = arr.unshift(unshifted);
		System.out.println(arr);
		System.out.println("Unshifted element: " + unshifted);
		System.out.println("Returns " + newArrLength2);
	}
}