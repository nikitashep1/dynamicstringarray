import java.util.Random;

public class DynamicStringArray {
	private String[] stringArr;
	
	public DynamicStringArray() {
		this.stringArr = new String[]{"one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten"};
		System.out.println("");
		System.out.println("  _____                              _         _____ _        _                                             ");
		System.out.println(" |  __ \\                            (_)       / ____| |      (_)                 /\\                         ");
		System.out.println(" | |  | |_   _ _ __   __ _ _ __ ___  _  ___  | (___ | |_ _ __ _ _ __   __ _     /  \\   _ __ _ __ __ _ _   _ ");
		System.out.println(" | |  | | | | | '_ \\ / _` | '_ ` _ \\| |/ __|  \\___ \\| __| '__| | '_ \\ / _` |   / /\\ \\ | '__| '__/ _` | | | |");
		System.out.println(" | |__| | |_| | | | | (_| | | | | | | | (__   ____) | |_| |  | | | | | (_| |  / ____ \\| |  | | | (_| | |_| |");
		System.out.println(" |_____/ \\__, |_| |_|\\__,_|_| |_| |_|_|\\___| |_____/ \\__|_|  |_|_| |_|\\__, | /_/    \\_\\_|  |_|  \\__,_|\\__, |");
		System.out.println("          __/ |                                                        __/ |                           __/ |");
		System.out.println("         |___/                                                        |___/                           |___/ ");
		System.out.println("");
	}
	
	public String toString() {
		String msg = "";
		for(int i = 0; i < this.stringArr.length; i++) {
			if (i == 0) {
				msg += ("{" + this.stringArr[i] + ", ");
			} else if (i == (this.stringArr.length - 1)) {
			msg += (this.stringArr[i] + "}");
			} else {
				msg += this.stringArr[i] + ", ";
			}
		}
		return msg;
	}
	
	public void reset() {
		this.stringArr = new String[]{"one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten"};
	}
	
	public void shuffle() {
		Random rand = new Random();
		int randomIndex;
		String firstElement;
		String secondElement;
		for (int i = 0; i < this.stringArr.length; i++) {
			randomIndex = rand.nextInt(0, (this.stringArr.length - 1));
			firstElement = this.stringArr[i];
			secondElement = this.stringArr[randomIndex];
			this.stringArr[i] = secondElement;
			this.stringArr[randomIndex] = firstElement;
		}
	}
	public String pop() {
		int newArrLength = (this.stringArr.length - 1);
		String lastElement = this.stringArr[newArrLength];
		String[] newStringArr = new String[newArrLength];
		for (int i = 0; i < newStringArr.length; i++) {
		newStringArr[i] = this.stringArr[i];	
		}
		this.stringArr = newStringArr;
		return lastElement;
	}
	public int push(String param) {
		int newArrLength = (this.stringArr.length + 1);
		String[] newStringArr = new String[newArrLength];
		for (int i = 0; i < newArrLength; i++) {
			if (i == (newArrLength - 1)) {
				newStringArr[i] = param;
			} else {
				newStringArr[i] = this.stringArr[i];
			}
		}
		this.stringArr = newStringArr;
		return newArrLength;
	}
	public String shift() {
		String firstElement = this.stringArr[0];
		int newArrLength = (this.stringArr.length - 1);
		String[] newStringArr = new String[newArrLength];
		int j;
		for (int i = 0; i < newStringArr.length; i++) {
			j = (i + 1);
			newStringArr[i] = this.stringArr[j];
		}
		this.stringArr = newStringArr;
		return firstElement;
	}
	
	public int unshift(String param) {
		int newArrLength = (this.stringArr.length + 1 );
		String[] newStringArr = new String[newArrLength];
		int j;
		newStringArr[0] = param;
		for (int i = 1; i < newStringArr.length; i++) {
			j = (i - 1);
			newStringArr[i] = this.stringArr[j];
		}
		this.stringArr = newStringArr;
		return newArrLength;
	}
}